"use strict";
const path = require("path");
const fs = require("fs");

const self = function(packageName) {
	let basePackageJson = null;

	if (packageName === null || packageName === undefined || typeof packageName !== "string") {
		throw new Error("'packageName' must be specified as a string.");
	}

	// Determine basePath
	let foundBasePath = null;
	let basePath = path.join(__dirname, "..");
	try {
		let dirFiles;
		while (dirFiles = fs.readdirSync(basePath)) {
			if (dirFiles.indexOf("package.json") !== -1 && (basePackageJson = JSON.parse(fs.readFileSync(path.join(basePath, "package.json"), "utf8"))).name === packageName) {
				foundBasePath = basePath;
				break;
			}

			basePath = path.join(basePath, "..");
		}
	}
	catch (ex) {
		throw new Error("[" + basePath + "] Unable to find package.json with packageName '" + packageName + "': " + (ex.message ? ex.message : JSON.stringify(ex)));
	}
	if (foundBasePath === null) {
		throw new Error("Unable to find package.json with packageName '" + packageName + "'.  Cannot continue.");
	}

	basePath = path.normalize(foundBasePath);

	const basePathRequire = function(relativePath, customRequireFn) {
		if (relativePath === null || relativePath === undefined || typeof relativePath !== "string") {
			throw new Error("'relativePath' must be specified as a string.");
		}

		if (!customRequireFn) {
			return require(path.join(basePath, relativePath));
		}
		else if (typeof(customRequireFn) === "function") {
			return customRequireFn(path.join(basePath, relativePath));
		}
		else {
			throw new Error("'customRequireFn', if provided, must be a function.");
		}
	};
	basePathRequire.getBasePath = function() {
		return basePath;
	};
	basePathRequire.pathFromBase = function(extraPath) {
		return path.join(basePath, extraPath);
	};
	basePathRequire.getBasePackageJson = function() {
		return basePackageJson;
	};

	// Now return function that requires what's passed in relative to the base path (as well as exposing detected base path through getBasePath())
	return basePathRequire;
};

module.exports = self;
