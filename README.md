# requirebase
Allows requiring modules relative to the base of the specified npm module (based on package.json 'name'), rather than relative to the current file.

The module assumes that "package.json" will be in the directory to be considered the "base" or "root" directory from which you wish to require files.  "node_modules" gets created underneath this directory by npm.

It also assumes that it will be installed somewhere under the "node_modules" directory.  That way, when searching up the directory chain for the appropriate "package.json", it must find the "package.json" in the base directory.

Examples of use:

    // Require and properly initialize requirebase module
    const requirebase = require("requirebase")("base-module-name");

	// Now require some library modules (in 'lib' subdir under base dir)
	const utilities = requirebase("lib/utilities");
	const parser = requirebase("lib/parser");

	// Require an MVC controller
	const ctrlUsers = requirebase("www/mvc/controllers/users");

	// Require a library module using a custom require() function
	const parserRewire = requirebase("lib/parser", rewire);

Once initialized by calling the requirebase function with the desired npm module name whose package.json to consider the root/base directory, you can get the detected base directory path:

	let baseDir = requirebase.getBasePath();

You can also construct a path from the combination of the detected base directory path and an extra specified path:

	let mvcDir = requirebase.pathFromBase("www/mvc");

In addition, to get info from the base package.json parsed into a JavaScript object (eg. version):

	let packageVersion = requirebase.getBasePackageJson().version;
