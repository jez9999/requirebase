// Type definitions for requirebase
// Definitions by: jez9999 <http://gooeysoftware.com/jsplugins/>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped
// TypeScript Version: 2.5.3

export = requirebase;

declare function requirebase(packageName: String): requirebase.RequireBaseReturnType;

declare namespace requirebase {
	export interface RequireBaseReturnType {
		(relativePath: string, customRequireFn?: (id: string) => any): any;
		getBasePath(): string;
		pathFromBase(extraPath: string): string;
		getBasePackageJson(): any;
	}
}
